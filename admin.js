
(function($) { 
    
    var id=0;
    var fadespeed=150;
    var moving=false;
    $(document).ready(function() {
        
        var $labelholder = $('.imagemap-cont');
        var $container = $('.imagemap-cont .overlay');
        var offset=$container.offset();
        var $coordsbox = $('<div>').addClass('coordsbox').append('href: <input type="text">'+
            ' <span class="keep" title="'+Drupal.t('Keep')+'">&radic;</span>'+
            ' <span class="remove" title="'+Drupal.t('Remove')+'">&times;</span>');
        
        
        //read saved coordinated into editor
        var coords=$('.hidden-coords').text().split(':');
        for(var i in coords){
            var ci=coords[i].split(',');
            if(ci.length==5){
                id++;
                var $selection = $('<div>').addClass('selection-box').append($coordsbox.clone())
        .append('<div class="clicker"></div>');
                $selection.css({
                    'left':   ci[0]+"px",
                    'top':    ci[1]+"px",
                    'width':  ci[2],
                    'height': ci[3]
                }).find('input').val(ci[4]);
                $selection.appendTo($labelholder).attr('id','link'+id);
                prepareCoords($selection);
            }
        }
        
        
        $container.mousedown(function(e) {
            moving=true;
            id++;
            $container.css('z-index',30);
            $('.coordsbox').fadeOut(fadespeed);
            var click_y = e.pageY-offset.top,
            click_x = e.pageX-offset.left;
            var $selection = $('<div>').addClass('selection-box').append($coordsbox.clone())
        .append('<div class="clicker"></div>');
            $selection.css({
                'left':   click_x,
                'top':    click_y,
                'width':  0,
                'height': 0
            });
            $selection.appendTo($labelholder).attr('id','link'+id);
        
        
            
            $container.mousemove(function(e) {            
                var move_x = e.pageX-offset.left,
                move_y = e.pageY-offset.top,
                width  = Math.abs(move_x - click_x),
                height = Math.abs(move_y - click_y),
                new_x, new_y;
          
                new_x = (move_x < click_x) ? (click_x - width) : click_x;
                new_y = (move_y < click_y) ? (click_y - height) : click_y;
          
                $selection.css({
                    'width': width,
                    'height': height,
                    'top': new_y,
                    'left': new_x
                });
          
            }).mouseup(function(e) {
                moving=false;
                $container.unbind('mousemove');
                $container.css('z-index',10);
                if($('#link'+id).width()<5||$('#link'+id).height()<5){
                    //treshold of size
                    $('#link'+id).remove();
                }else{
                    $('#link'+id).children().fadeIn(fadespeed);
                    prepareCoords($selection);
                }
            }).mouseout(function(e){
                if(moving){
                $(this).mouseup();
                }
            });
        });
       
       $('#edit-submit').click(function(){
           updateCoords();
       });
       
    });
    
    function updateCoords(){
        var links=$('.selection-box');
        var data = '';
        links.each(function(){
            var pos=$(this).position();
            var w=$(this).width();
            var h=$(this).height();
            var i=$(this).find('input').val();
            data+=pos.left+','+pos.top+','+w+','+h+','+i+':';
        });
        $('.hidden-coords').text(data);
    
    
    }
    
    function prepareCoords(o) {
        $('.keep',o).click(function(){
            $('.coordsbox',o).fadeOut(fadespeed);
        });
        $('.remove',o).click(function(){
            o.fadeOut(fadespeed,function(){
                o.remove();
            });
        });
        $('.clicker',o).click(function(){
            console.log('at');
            $('.selection-box').css('z-index',20);
            $('.coordsbox').fadeOut(fadespeed);
            var par = $(this).parent();
            par.css('z-index',21);
            par.children('.coordsbox').stop().show();
        });
    };

})(jQuery);
