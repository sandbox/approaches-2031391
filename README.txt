This module creates one block to be configured through the blocks interface.
The block contains an image with a configurable interface to add image map links (only rectangles for now)
Some tokens can be used through the linking

In this version, after uploading an image, the block needs to be saved first before proceeding.
Didn't bother yet to figure out how to do this more smoothly with some AJAX.